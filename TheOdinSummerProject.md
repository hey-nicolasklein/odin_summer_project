# The Odin Summer Project

## What is this?

The Odin Project combines many WebDev online learning resources to get one started in that field.

This summary resambles my personal notes as I do this cours this summer. I know its already really late summer, but my exames needed my whole attention up to this point. Right now I'm feeling I need to tip my toes in a really new field. Sadly my university doesn't teach us much web development. Therefor I'm on my own to develop these skills. 

Because I'm living by the learn, sum and test principle I want to sum my learnings as I go. And that's what this document is about.

## Motivation

### Why?

- Do you want to have a fulfilling career that pays well?
- Are you excited by the creative outlet programming provides?

### When it gets hard

*If it gets* [*hard*](https://techcrunch.com/2014/05/24/dont-believe-anyone-who-tells-you-learning-to-code-is-easy/?guccounter=1).

*Struggling with something is growth. It doesn’t matter how long you struggle with a concept or project; all that matters is that you have the grit and tenacity to see it through. That’s how real learning happens.*

Blieve is [key](https://www.ted.com/talks/angela_lee_duckworth_grit_the_power_of_passion_and_perseverance).

### How to learn

-> In short, understand it, practice it, and finally teach it. [Coursera Lessons](https://www.coursera.org/learn/learning-how-to-learn)

### Pitfalls

+ Procrastination -> Pomodoro Timer 
+ Not taking breaks -> Pomodoro Timer
+ Dont waste time going in rabbit holes.
+ Dont compare yourself to others -> Only compare yourself to your past self. Have your abilities and knowledge improved from where you were last week, last month, or last year? Be proud of the progress that you’ve made! 

### Bonus

Pump up your typing skills: https://klava.org/

## Getting Started

*Skipped setting up OS, Brew, Git, VSCode ...here for simplicity*

```Bash 
#Check if SSH Key already has been generated
ls ~/.ssh/id_rsa.pub

#If not, generate one
ssh-keygen -C <youremail>

#Cat our ssh-key so we can copy/paste it to github/gitlab
cat ~/.ssh/id_rsa.pub
```

 

## First Rails App

```bash
#Used to initialize a new rails project in current folder
rails new my_first_rails_app

#Next we generate some starter project to verify our installation
rails generate scaffold car make:string model:string year:integer

#Now we have to migrate our database
rails db:migrate

#Finally start our server
rails server
```

```bash
#To add a remote repository to an already existing local one use
git remote add origin <SSH URL from above>
git push -u origin master
```

````bash
#When Gem-File changed
bundle install --without production

#Push new Version to Heroku
git push heroku master

````

````bash
#Heroku Setup
heroku create
#Push new changes to Heroku
git push heroku master
#Run Migration and open in browser
heroku run rails db:migrate
heroku open
````

## All things Git

````bash
#If you forgot to add a file in last commit
git commit -m 'initial commit'
git add forgotten_file
git commit --amend

#Unstage a single file
git reset HEAD file.c

#Undo changes to a file before commit
git checkout -- file.c

#Display what Git did recently
git log --oneline --decorate
git log --oneline --decorate --graph --all

#Adding new branch and switching to it
git checkout -b <newbranchname>

#Deleting branches
git branch -d hotfix

#Merging
git checkout master
git merge <branchname>
````

### Branching

````bash
git branch -v #Showes all branches with last commit message

#Showes which branches are merged with regard to your CURRENT branch.
git branch --merged
git branch --no-merged

#Both take any branch as an argument
git branch --merged featureB
````

### Merge Conflicts

````bash
#git status tells you which files are problematic
$ git status
On branch master
You have unmerged paths.
  (fix conflicts and run "git commit")

Unmerged paths:
  (use "git add <file>..." to mark resolution)

    both modified:      index.html

no changes added to commit (use "git add" and/or "git commit -a")

#Files containing problems look like this
#The top part (anything above ======) is the branch you were on when calling git merge. This branch head the HEAD pointer point to.
<<<<<<< HEAD:index.html
<div id="footer">contact : email.support@github.com</div>
=======
<div id="footer">
 please contact us at support@github.com
</div>
>>>>>>> iss53:index.html


#To resolv the issue change the file in the way you want and
git add problematic.c

#Or use a merge tool
git mergetool

#Finally mark the conflict as solved with
git commit #The commit message is automaticly created

````

## Webdevelopment Basics

### Naming

lower-case-with-dashes.jpg

### Basic folder-struckture of a website

+ index.html
+ images folder
+ styles folder
+ Scripts folder

### Basic HTML-Structure

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>My test page</title>
  </head>
  <body>
    <img src="images/firefox-icon.png" alt="My test image">
  </body>
</html>
```

+ head -> Everything that isn't content you show on your website (eg. Keywords or page descriptions)
+ Body -> Stuff that is displayed to the user

````html
<img src="images/firefox-icon.png" alt="My test image">

<h1>My main title</h1>
<h2>My top level heading</h2>
<h3>My subheading</h3>
<h4>My sub-subheading</h4>

<p>This is a single paragraph</p>

<ul> 
  <li>technologists</li>
  <li>thinkers</li>
  <li>builders</li>
</ul>

<a href="https://www.mozilla.org/en-US/about/manifesto/">Mozilla Manifesto</a>

<form action="#" method="POST" name="my_form">
  First name:<br>
  <input type="text" name="user" value="Mickey"><br>
  Last name:<br>
  <input type="text" name="lastname" value="Mouse"><br><br>
  <input type="submit" value="Submit">
</form>
````

### Basic CSS

-> *Stylesheet Language* = You apply different styles to different elements in HTML documents.

##### Link a stylesheet 

````html
--> Inline CSS
<p style="color: red">text</p>

--> Internal CSS
<!DOCTYPE html>
<html>
	<head>
		<title>CSS Example</title>
		<style>

      p {
          color: red;
      }

      a {
          color: blue;
      }

		</style>
...
   
--> External CSS
<!DOCTYPE html>
<html>
<head>
    <title>CSS Example</title>
    <link rel="stylesheet" href="style.css">
...
  
````

##### The selector

![image-20190818215757577](/Users/nicolas/Development/odin_summer_project/Resources/cssfuntionality.png)

| Selector name                                              | What does it select                                          | Example                                                      |
| :--------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| Element selector (sometimes called a tag or type selector) | All HTML element(s) of the specified type.                   | `p` Selects `<p>`                                            |
| ID selector                                                | The element on the page with the specified ID. On a given HTML page, **you're only allowed one element per ID** (and of course one ID per element). | `#my-id` Selects `<p id="my-id">` or `<a id="my-id">`        |
| Class selector                                             | The element(s) on the page with the specified class (multiple class instances can appear on a page). | `.my-class` Selects `<p class="my-class">`and `<a class="my-class">` |
| Attribute selector                                         | The element(s) on the page with the specified attribute.     | `img[src]` Selects `<img src="myimage.png">` but not `<img>` |
| Pseudo-class selector                                      | The specified element(s), but only when in the specified state (e.g. being hovered over). | `a:hover` Selects `<a>`, but only when the mouse pointer is hovering over the link. |

##### Google Fonts

````html

<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> -> used for Google Fonts
````

````css
html {
  font-size: 10px; /* px means "pixels": the base font size is now 10 pixels high  */
  font-family: "Open Sans", sans-serif; /* this should be the rest of the output you got from Google fonts */
}
````

##### padding, margin and border

- padding`, the space just around the content (e.g., around paragraph text).
- `border`, the solid line that sits just outside the padding.
- `margin`, the space around the outside of the element.

![image-20190818220318928](/Users/nicolas/Development/odin_summer_project/Resources/paddingbordermargin.png)

##### Pseudo-Classes

`````css
a:active {
    color: red;
}

a:hover {
    text-decoration: none;
    color: blue;
    background-color: yellow;
}

input:focus, textarea:focus {
    background: #eee;
}
`````

##### The position property

````css
/* static - the default*/
p{
  position: static;
}

/* relative - relative to static position */
p{
  position: relative;
  top: 20px;
  bottom: 40px;
}

/* absolute - relative to closest parent, isn't in html flow anymore */
p{
  position: absolute;
  
}

/* fixed - will stay fixed at position, even when scrolling, isn't in html flow anymore */
p{
  position: fixed;
  top: 20px;
  left: 50px;
}
````

##### Z-Index

````css
/* Greater value: more in foreground */
.header{
  z-index: 10;
  position: fixed;
  width: 100%;
}
````

##### The float property

````css
/* Brings the box as far right as possible */
div{
  float: right;
}

/* The specified side will not touch any other element within the same containing element */
div.special{
  clear: left; //left, right, both, none
}
````



##### The display property

````css
/* inline - No linebreak after element */
h1{
  display: inline;
}

/* block - linebreak after element */
a{
  display: block;
}

/* inline-block - On one line with width and heigth properties */
a.blockLine{
  display: inline-block;
}
````

##### Grid-template property

````css
.grid{
  	display: grid;
		width: 1000px;
		height: 500px;
    grid-template-areas: "header header" "nav nav" "left right" "footer footer";
  	grid-template-columns: 200px 400px;
  	grid-template-rows: 150px 200px 600px 200px;
  	grid-gap: 20px 5px;
  
  	/*
  	grid-template-rows: 10% 20% 600px;
  	grid-template: repeat(3, 1fr) / 3fr minmax(50px,300px) 1fr;
		*/
}



.item {
  /*--> row-start / column-start / row-end / column-end*/   
  grid-area: 2 / 3 / 4 / span 5;
}
	


grid-gap: 10px;
````

##### Grid-template-areas

````css
grid-template-areas: "head head"
                       "nav nav" 
                       "info services"
                       "footer footer";
nav {
  grid-area: nav;
} 
````

##### Overlapping Elements

`````css
.overlap {
  background-color: lightcoral;
  grid-area: 6 / 4 / 8 / 6;
  z-index: 5;
}
`````

##### Justify Items

We can use `justify-content` to position elements along **the row axis**.

`justify-items` accepts these values:

- `start` — aligns grid items to the left side of the grid area
- `end` — aligns grid items to the right side of the grid area
- `center` — aligns grid items to the center of the grid area
- `stretch` — stretches all items to fill the grid area

Without justify-items each item is as wide as the column. By setting both justify-items and a width on the item itself we can change that. 

##### Justify Content

We can use `justify-content` to position the entire grid along **the row axis**.

It accepts these values:

- `start` — aligns the grid to the left side of the grid container
- `end` — aligns the grid to the right side of the grid container
- `center` — centers the grid horizontally in the grid container
- `stretch` — stretches the grid items to increase the size of the grid to expand horizontally across the container
- `space-around` — includes an equal amount of space on each side of a grid element, resulting in double the amount of space between elements as there is before the first and after the last element
- `space-between` — includes an equal amount of space between grid items and no space at either end
- `space-evenly` — places an even amount of space between grid items and at either end

##### Align Items

Aligns items from **top to bottom.**

`align-items` accepts these values:

- `start` — aligns grid items to the top side of the grid area
- `end` — aligns grid items to the bottom side of the grid area
- `center` — aligns grid items to the center of the grid area
- `stretch` — stretches all items to fill the grid area

##### Align Content

Aligns all content  from **top to bottom.**

It accepts these positional values:

- `start` — aligns the grid to the top of the grid container
- `end` — aligns the grid to the bottom of the grid container
- `center` — centers the grid vertically in the grid container
- `stretch` — stretches the grid items to increase the size of the grid to expand vertically across the container
- `space-around` — includes an equal amount of space on each side of a grid element, resulting in double the amount of space between elements as there is before the first and after the last element
- `space-between` — includes an equal amount of space between grid items and no space at either end
- `space-evenly` — places an even amount of space between grid items and at either end

##### Align self and justify self

Align-self and justify-self override what ever value was set with align-items and justify-items.

They both accept these four properties: 

- `start` — positions grid items on the left side/top of the grid area
- `end` — positions grid items on the right side/bottom of the grid area
- `center` — positions grid items on the center of the grid area
- `stretch` — positions grid items to fill the grid area (default)

##### Grid Auto Rows and Grid Auto Columns

If the ammount of content is unknown a designer can specify a default height and width of extra columns.

````css
body {
  display: grid;
  grid: repeat(2, 100px) / repeat(2, 150px); 
  grid-auto-rows: 50px;
}
````

**grid-auto-rows** and **grid-auto-columns** accept the same values as their explicit counterparts, **grid-template-rows** and **grid-template-columns**.

##### Grid Auto Flow

`grid-auto-flow` specifies whether new elements should be added to rows or columns.

`grid-auto-flow` accepts these values:

- `row` — specifies the new elements should fill rows from left to right and create new rows when there are too many elements (default)
- `column` — specifies the new elements should fill columns from top to bottom and create new columns when there are too many elements
- `dense` — this keyword invokes an algorithm that attempts to fill holes earlier in the grid layout if smaller elements are added

You can pair `row` and `column` with `dense`, like this: `grid-auto-flow: row dense;`.

#### Userconfigured properties in CSS

```css
:root {
    --primary-color: #59acfa;
    --primary-dark-color: #2c6fad;
    --medium-margin: 20px;
}

h1 {
    color: var(--primary-color);
}
```

Userconfigured properties can also be assigned in css classes. 

```css
main {
    --p-size: 20pt;
}

aside {
    --p-size: 30pt;
}

p {
    font-size: var(--p-size);
}
```

The font-size of p elements  now matches their parent ``--p-size ` value.

### JavaScript Basics

````html
--> Just before </body> tag
<script src="scripts/main.js"></script>

--> Thats because the html is loaded top to bottom
and in javascript referenced html tags may not exist 
at this time.
````

````javascript
let myvar = 'Nicolas' //Always use let when possible. Let isn't making the variable constant.
let myVariable = [1,'Bob','Steve',10];
let myVariable = document.querySelector('h1');

myVar === 4 //Equality

function multiply(num1,num2) {	//Function
  let result = num1 * num2;
  return result;
}

multiply(4, 7); //Function-call

document.querySelector('html').onclick = function() { //Event
    alert('Ouch! Stop poking me!');
}

localStorage.setItem('name', myName); //Saves myName
localStorage.getItem('name') //Loads myName

alert("My alert message") //Alert-Popup
let myName = prompt('Please enter your name.'); //Input from user popup
````

Open devtools -> *CMD + Option + I*

#### Numbers in Javascript

Any type of variable gets converted to a number with a acending **+ (plus)** Operator. For example 

```javascript
// Converts non-numbers
alert( +true ); // 1
alert( +"" );   // 0
```

Also new is the **Exponentiation** ****** in Javacript

````javascript
alert( 2 ** 2 ); // 4 = (2 * 2)
alert( 2 ** 4 ); // 16 = (2 * 2 * 2 * 2)

alert( 4 ** (1/2) ); // 2 (power of 1/2 is the same as a square root, that's maths)
alert( 8 ** (1/3) ); // 2 (power of 1/3 is the same as a cubic root)

````

Comparisson of strings is done dictionary vise, hence

````javascript
"apple" > "pineapple" → false // a and p are compared
"2" > "12" → true // 2 and 1 are compared
````

Undefined vs nul

````javascript
undefined == null → true // equal each other
undefined === null → false // but are not exact same type
````

**Let, const and the old var**, var shouldn't be used anymore.

````javascript
if (true) {
  var global = true; 
  let block = true;
}

alert(global); // true
alert(block); // error


//Also there is the “hoisting” (raising) of var
//Every var variable is declared at function start, but not assigned

const myBirthday = "05.10.1998" //cannot be changed
````

Functions and anonymous functions (methods later)

```javascript
//This is a function to generate a random number
function random(number) {
  return Math.floor(Math.random()*number);
}

//This is an anonymous function
var myButton = document.querySelector('button');
myButton.onclick = function() {
  alert('hello');
}

//anonymous functions can be stored in a variable
var myGreeting = function() {
  alert('hello');
}
```

Functions as parameters 

```javascript
let confirmYes = function() {
  alert( "yes" );
};

let confirmNo = function() {
  alert( "no" );
};

function ask(question, yes, no) {
  if (confirm(question)) yes()
  else no();
}

ask("Is peter nice?", confirmYes, confirmNo)

//confirmYes and No are called callBackFunctions
```

Default function parameters

```javascript
function showMessage(sender, text="This is a default text"){
  let message = "Message From: " + sender + "as: " + text;
  alert(message)
}

function showMessage(from, text = anotherFunction()) {
  // anotherFunction() only executed if no text given
  // its result becomes the value of text
}
```

Common mistakes

```javascript
function doNothing() {
  return
 (some + long + expression + or + whatever * f(a) + f(b))
}

//this would simply return undefined
//js assumes a ; after return and quites the function
// => avoid new lines after return
```

 Modifing an array

`````javascript
//Get index of specific element
result.indexOf(arguments[i]);

//Remove element at "index"
//Returnes deleted element(s)
result.splice(index,1);

//Convert Array to String
let strResult = result.join("");
`````

Functions with an arbitrary ammount of arguments

````javascript
const removeFromArray = function(arr){
  //Ammount of arguments
  if arguments.length -1 ? true : false;
  ...
  //Access to arguments
 	let index = result.indexOf(arguments[i]);
  ...
}
````

Strings in JS

`````javascript
//Concat two strings
result = result.concat(text);

//Get char
char = result.charAt(5);

//Lowercase
result = result.toLowerCase();
`````

### DOM Manipulation

DOM (Document Object Model) is a tree-like representation of a website. The relationships depent on the order of the html document.

#### Targeting nodes with selectors

HTML example:

```html
<div id="container">
  <div class="display"></div>
  <div class="controls"></div>
</div>
```

To refer to div we could use either:

+ div.display
+ .display
+ \#container>.display
+ div#container>div.display

We could also use **relational selectors**, in code this would lead to:

```javascript
const container = document.querySelector('#container');
// select the #container div (don't worry about the syntax, we'll get there)

console.dir(container.firstElementChild);                      
// select the first child of #container => .display

const controls = document.querySelector('.controls');   
// select the .controls div

console.dir(controls.previousElementSibling);                  
// selects the prior sibling => .display
```

###DOM methods

#### Query selectors

- *element*.querySelector(*selector*) returns reference to the first match of *selector*
- *element*.querySelectorAll(*selectors*) returns a “nodelist” containing references to all of the matches of the *selectors*

#### Element selection

```javascript
const div = document.createElement('div');
```

This doesn't put the element in the DOM. It just creates it in memory.

##### Append elements

- *parentNode*.appendChild(*childNode*) appends *childNode* as the last child of *parentNode*
- *parentNode*.insertBefore(*newNode*, *referenceNode*) inserts *newNode* into *parentNode* before *referenceNode*

#### Remove elements

+ *parentNode*.removeChild(*child*) removes *child* from *parentNode* on the DOM and returns reference to *child*

#### Adding inline style

```javascript
div.style.color = 'blue';                                      
// adds the indicated style rule

div.style.cssText = 'color: blue; background: white';          
// adds several style rules

div.setAttribute('style', 'color: blue; background: white');    
// adds several style rules
```

#### Editing attributes

```javascript
div.setAttribute('id', 'theDiv');                              
// if id exists update it to 'theDiv' else create an id
// with value "theDiv"

div.getAttribute('id');                                        
// returns value of specified attribute, in this case
// "theDiv"

div.removeAttribute('id');                                     
// removes specified attribute
```

#### Working with classes

```javascript
div.classList.add('new');                                      
// adds class "new" to your new div

div.classList.remove('new');                                   
// remove "new" class from div

div.classList.toggle('active');                                
// if div doesn't have class "active" then add it, or if
// it does, then remove it
```

#### Adding text content

```javascript
div.classList.add('new');                                      
// adds class "new" to your new div

div.classList.remove('new');                                   
// remove "new" class from div

div.classList.toggle('active');                                
// if div doesn't have class "active" then add it, or if
// it does, then remove it
```

### Events in JS

#### Method 1

```html
<button onclick="alert('Hello World')">Click Me</button>
```

#### Method 2

```html
<!-- the html file -->
<button id="btn">Click Me</button>
```

```javascript
// the JavaScript file
var btn = document.querySelector('#btn');
btn.onclick = () => alert("Hello World");
```

#### Method 3 (preferred)

````html
<!-- the html file -->
<button id="btn">Click Me</button>
````

```javascript
// the JavaScript file
var btn = document.querySelector('#btn');
btn.addEventListener('click', () => {
  alert("Hello World");
});
```

#### Using named functions

```javascript
function alertFunction() {
  alert("YAY! YOU DID IT!");
}

// METHOD 2
btn.onclick = alertFunction;

// METHOD 3
btn.addEventListener('click', alertFunction);
```

#### More Information about the event

```javascript
//adding e as function parameter
btn.addEventListener('click', function(e){
  e.target.style.background = 'blue';
})
```

#### Select more than one node

```html
<div id="container">
    <button id="1">Click Me</button>
    <button id="2">Click Me</button>
    <button id="3">Click Me</button>
</div>
```

```javascript
// buttons is a node list. It looks and acts much like an array.
const buttons = document.querySelectorAll('button');

// we use the .forEach method to iterate through each button
buttons.forEach((button) => {

  // and for each one we add a 'click' listener
  button.addEventListener('click', (e) => {
    alert(button.id);
  });
});
```

#### Listening for non element specific events

````javascript
window.addEventListener('keydown', function (e){
 		//do stuff 
});
````

#### Searching for specific html elements

````html
...
<body>
  <audio data-key="65" src ="sounds/some.mp3"></audio>
  <div data-key="70" class="key">
      <kbd>F</kbd>
      <span class="sound">openhat</span>
  </div>
</body>
<script>
	window.addEventListener('keydown', function(e){
    //`this is a template String`
    const audio = document.querySelector(`audio[data-key="${e.keyCode}"]`);
   	const key = document.querySelector(`.key[data-key="${e.keyCode}"]`);
    
    console.log(audio);
  })
</script>
````

#### Template Strings

``````javascript
`string text ${expression} string text`

var a = 5;
var b = 10;
//not easy to read
console.log("Fifteen is " + (a + b) + " and\nnot " + (2 * a + b) + ".");
//easy to read
console.log(`Fifteen is ${a + b} and\nnot ${2 * a + b}.`);
``````

### Objects in Javascript

```javascript
let user = new Object(); // "object constructor" syntax
let user = {};  // "object literal" syntax
```

Remove property

```javascript
delete user.age;
```

There are **multiword properties**

```javascript
let user = {
  name: "John",
  age: 30,
  "likes birds": true  // multiword property name must be quoted
};
```

acess them with

```javascript
// set
user["likes birds"] = true;

// get
alert(user["likes birds"]); // true

// delete
delete user["likes birds"];
```

also as a variable

```javascript
let key = "likes birds";

// same as user["likes birds"] = true;
user[key] = true;
```

#### Computed variables

```javascript
let fruit = prompt("Which fruit to buy?", "apple");

let bag = {
  [fruit]: 5, // the name of the property is taken from the variable fruit
};

alert( bag.apple ); // 5 if fruit="apple"
```

```javascript
let fruit = 'apple';
let bag = {
  [fruit + 'Computers']: 5 // bag.appleComputers = 5
};
```

#### Shorthand property values

```javascript
function makeUser(name, age) {
  return {
    name: name,
    age: age
    // ...other properties
  };
}
```

can be simplified to:

```javascript
function makeUser(name, age) {
  return {
    name, // same as name: name
    age   // same as age: age
    // ...
    secNumber: 40, //normal property
  };
}
```

### Existence check

```javascript
"key" in object
```

### The special object "for...in" loop

```javascript
for (key in object) {
  // executes the body for each key among object properties
}
```

```javascript
for (let key in user) {
  // keys
  alert( key );  // name, age, isAdmin
  // values for the keys
  alert( user[key] ); // John, 30, true
}
```

### Copying by reference

```javascript
let user = { name: "John" };

let admin = user; // copy the reference
```

### Cloning objects

```javascript
let clone = {}; // the new empty object

// let's copy all user properties into it
for (let key in user) {
  clone[key] = user[key];
}
```

Simpler way to do this:

```javascript
Object.assign(dest, [src1, src2, src3...])
```

 assign() uses the exact same technique. All properties from src1, ... srcN are copied to dest.

If there already is a property with the same name as a new, the old one is overridden. 

For a simple clone, this use is appropriate:

```javascript
let clone = Object.assign({}, user);
```

### Array: Filter, Map, Sort & Reduce - The holy four

#### Filter

`````javascript
const result = students.filter(function(student){
	if(student.age > 20){
		return true
	}
  //No else needed here
})
`````

Bonus: **console.table()** logs objects in a table notation.

### Map

Takes array and returns a new array of the same length.

`````javascript
const fullnames = students.map(student => student.first + student.last)
`````

#### Sort

Return 1 for greater and -1 for smaller

```javascript
const ordered = student.sort((a,b) => {
  if(a > b){
    return 1;
  }
  else{
    return -1;
  }
})
```

#### Reduce

```javascript
const result = students.reduce ((total, student) =>{
  return total+= student.money;
}, 0)

//Last line 0 is used in the first run, in which we have no value for total
```

### Convert an nodeList to an array

```javascript
const arr = Array.from(document.querySelector("#nodes"))
```

### Little gems here and there

````javascript
console.table //logs objects in a table notation

console.log({myVariable}) //prints name and value (not only the value)
````

### New Array functions

+ const anyNumber =  myArray.some(a => typeOf a == "number");
+ const allNumber =  myArray.every(a => typeOf a == "number");
+ const myComment = myArray.find(comment => comment == "me :)");
  + Returns only **first find**, othervise its like filter.
+ const index = myArray.index(comment => comment == "me :)");
  + Returns the index in the array of the found result
  + Delete now with **myArray.splice(index,1);**

## Ruby Basics

```ruby
#Prints Hello 3 times

puts "Hello"*3
```

### Variables

````ruby
myString = "Its me Mario"
puts myString


myString = "5"
myInt = 4

#Variable conversion
puts myString.to_i + myInt
puts myString + myInt.to_s

#Get input
gets

#Remove enter after string
gets.chomp

#String methods
myString.reverse
myString.upcase.downcase.swapcase.capitalize
myString.center(lineWidth)
````

### Flow Control

````ruby
if name == 'Chris'
  puts "yala"
else
  puts ":\/)"
end


while command != 'bye'
  puts command
  command = gets.chomp
end

if iAmChris and iLikeFood
  puts "nice"
end
````

### Arrays

`````ruby
[5]
words = ["Hello", "Goodbye"]

puts words[0]


puts words [2] #Will print nothing, which is nil here

#Array method
words.each do |word|
  puts word
end

#Integer method
3.times do
  puts 'Hip-Hip-Hooray!'
end

#More Array methods

word.to_s
words.join(", ")
words.push 'Yeah'
words.pop
words.last
words.length


`````

## Methods

````ruby
def doubletThis num
  numTimes2 = num*2
  
  numTimes2 #return optional here
end

def doubleThisWithoutReturn num
  num*2
end
````

## Classes

```ruby
#Extending Classes
class Integer
  def to_eng
    if self == 5
      english = 'five'
    else
      english = 'fifty-eight'
    end

    english
  end
end

#The time class
c = Time.new

#The Hash class
myHash = {}
myHash['me'] = 'nico'
myHash['notMe'] = 'peter'

myHash.each do |codeType, name|
  puts codeTpye +" : "+name
end


#Own class
class Person
  
  def initialize
    age
  end

  def age
    @age = Time.new
  end
  
  def getAge
		@age #Instance variable, live as long as the object does
  end
end
    
```

## Closures in Ruby

````ruby
#Base example
toast = Proc.new do
  puts 'Cheers!'
end

toast.call

#Iterate through parameters
doYouLike = Proc.new do |aGoodThing|
  puts 'I *really* like '+aGoodThing+'!'
end

doYouLike.call 'chocolate'

#Proc as parameter
def runProg someProc
  puts "Now it starts"
  someProc
  puts "Now it ends"
end

#Return Procs
def compose proc1, proc2
  Proc.new do |x|
    proc2.call(proc1.call(x))
  end
end

newproc = compose squeareIt, doubleIt

puts newproc.call(10)




myproc = Proc.new do |parameter|
  puts parameter.to_s
end
````

## JS ES6

### Destructuring Arrays and Objects

`````javascript
//Destructure of Arrays
let person = [Peter, Millark, Everdeen];

let [firstName, middleName, lastName] = person;

console.log(firstName);

//Destructure of objects
let person = {
  	firstName: "Peter",
  	lastName: "Millark"
};

let {firstName: fn, lastName: ln} = person;

console.log(fn);
`````

### Object Literals

```javascript
function updateAdress (city, state){
  let newAddr = {city, state};
  //Instead of = {city: city, state: state} or
  //Instead of = {myCity: city, myState: state}
  return newAddr
}
```

```javascript
//Combining all of the new features

function adressMaker (address){
  const {city, state} = adress;
  
  const newAddr = {
    city: city,
    state: state,
    country: "Germany"
  }
  
  console.log(`${newAddr.city} and ${newAddr.state}`)
}

const addr = {city: "Saarlous", state:"Saarland"};

adressMaker(addr)
```

### For-Loop

````javascript
let incomes = [123123,213123,123123,123123]
let total = 0;

for (let icome of incomes){
  total += income;
  console.log(income);
}


let name = "Nicole Little";

for(const char of name){
  console.log(char);
}
````

### Unwrapping Values

```javascript
let values = ["Peter", "Nico", "Millark"];
let newArr = [...values];
```

### Rest Operator

`````javascript
//Basically the oposite of the last feature

function printMyStuff(...elements){
  elements.forEach(e => console.log(e));
}

printMyStuff(5,5,234,23,324,32,4);
`````

## React

This is not part of the odin project. But as I'm interested in it I thought here is the right place to take notes. While I'm working through the iterative tutorial [here](reactjs.org) my notes are organized here. So lets render some JSX :)

To create a new JS Project use:

`````bash
npx create-react-app my-app
cd my-app
npm start
`````

This assumes Node >=8.10 and npm >=5.6

### Smallest Rest Snippet

```react
ReactDOM.render(
  <h1>Hello, world!</h1>,
  document.getElementById('root')
);
```

 ### JSX

`````react
const elemet = <h1>Nicolas Klein</h1>;
`````

This is not html and not even a String, this is JSX.

It is also possible to write JavaScript directly in JSX:

````react
const name = "Drake and Josh";
const elemet = <h1>Our names are {name}</h1>
      
ReactDOM.render(
	element,
  document.getElementById('root')
)
````

JSX elements can also contain childrens

`````react
const element = (
	<div>
  	<h1>Peter</h1>
    <h2>Parker</h2>
  </div>
)
`````

All JSX code is compiled down to React.createElement() calls

```react
const element = (
  <h1 className="greeting">
    Hello, world!
  </h1>
);
```

same as ...

```react
const element = React.createElement(
  'h1',
  {className: 'greeting'},
  'Hello, world!'
);
```

These objects are called **elements** and React reads those to build the DOM.

### Rendering Elements

To render a single element use

````react
const element = <h1>Hello, world</h1>;
ReactDOM.render(element, document.getElementById('root'))
````

"root" is the div in your html where your react content shall be displayed.

### Updating elements

Each element is like a single frame in a movie. Therefor it cannot be mutated. For now if you want to update the ui you have to create a new element and pass it to  the ``ReactDOM.render()``.

### Components

Components are like JavaScript functions.

arbitrary input (props) => element(s)

The simplest component is something like

`````react
function Welcome(props){
  return <h1>Nicolas, {props.lastName}</h1>;
}

//or

class Welcome extends React.Component{
  render(){
    return <h1>Nicolas, {props.lastName}</h1>;
  }
}
`````

Now to use Welcome we can write something like this 

````react
const element = <Welcome lastName="Klein"/>;
````

We can also pass direct JSX to the render method

````react
ReactDOM.render(<Welcome />, document.getElementById('root'));
````

Props are read only. Functions that not modify them are called "pure". 

**All react components must act like pure functions with respect to their props.**

### States

```react
class Clock extends React.Component {
    constructor(props) {
      super(props);
      this.state = {date: new Date()};
    }

  	//runs when component is added to DOM
    componentDidMount(){
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }
		
  	//runs when deleted from DOM
    componentWillUnmount(){
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            date: new Date()
        });
    }
  
    render() {
      return (
        <div>
          <h1>Hello, world!</h1>
          <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
        </div>
      );
    }
  }
```

### How to use state correctly

#### Do not modify state directly

Only the place where you shall use `this.state.anything = "something"` is in the constructor. Everywhere als use

````react
this.setState({comment: 'Hello'});
````

### State updates may be asynchronous

```react
// Correct
this.setState((state, props) => ({
  counter: state.counter + props.increment
}));
```

### State updates are merged

If your state contains many independet variables only the one you are stating in setState() are changed. Others are merged with their old value.

```react
constructor(props) {
    super(props);
    this.state = {
      posts: [],
      comments: []
    };
 }

//only state.posts is changed
componentDidMount() {
    fetchPosts().then(response => {
      this.setState({
        posts: response.posts
      });
    });

//only state.comments is changed
    fetchComments().then(response => {
      this.setState({
        comments: response.comments
      });
    });
  }
```

### Data flows down

Only the component has access to its state. However it is possible to pass state variables down to containing components

```react
<h2>It is {this.state.date.toLocaleTimeString()}.</h2>
```

 ### Event Listeners

````react
//Simple event listener, activateLaser is a function
<button onClick={activateLasers}>
  Activate Lasers
</button>

//In combination with JSX
function ActionLink() {
  function handleClick(e) {
    //prevents that a new page is opened
    e.preventDefault();
    console.log('The link was clicked.');
  }

  return (
    <a href="#" onClick={handleClick}>
      Click me
    </a>
  );
}
````

in React you dont call `addEventListener` after you created the element. Instead you add an listener when the component is first rendered. 

```react
class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: true};

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
  }

  render() {
    return (
      <button onClick={this.handleClick}>
        {this.state.isToggleOn ? 'ON' : 'OFF'}
      </button>
    );
  }
}

ReactDOM.render(
  <Toggle />,
  document.getElementById('root')
);
```

 The binding of this is needed, because in JS class methods are not bound by default. If we don't do it, its undefined.

But there are severall methods to overcome this

````react
handleClick = () => {
    console.log('this is:', this);
}
````

Sets `this` automatically. 

  ### Conditional Rendering

````react
//Its as simple as

function AdminGreeting(props){
  return (<h1>"Hello master!"</h1>)
}
function UserGreeting(props){
  return (<h1>"Say Hello to the system hahaha"</h1>)
}

let header = <h1>We welcome you</h1>

function ConditonalGreeting(props){
  if(props.userLevel != "admin"){
    return (
      {header}
      <UserGreeting />
    )
  }
  else
    return (<AdminGreeting />)
}

ReactDOM.render(<ConditionalGreeting userLevel="admin" />, document.getElementById('root'))
````

### Inline conditional rendering

```react
function Mailbox(props) {
  const unreadMessages = props.unreadMessages;
  return (
    <div>
      <h1>Hello!</h1>
      {unreadMessages.length > 0 &&
        <h2>
          You have {unreadMessages.length} unread messages.
        </h2>
      }
    </div>
  );
}

const messages = ['React', 'Re: React', 'Re:Re: React'];
ReactDOM.render(
  <Mailbox unreadMessages={messages} />,
  document.getElementById('root')
);
```

`true && expression == expression`

`false && expression == false`

Therefor the last one simple won't get printed by React.

Also the conditional statement can be used with JSX.

```react
...
<div>
      The user is <b>{isLoggedIn ? 'currently' : 'not'}</b> 			logged in.
</div>
...

//or

...
<div>
      {isLoggedIn ? (
        <LogoutButton onClick={this.handleLogoutClick} />
      ) : (
        <LoginButton onClick={this.handleLoginClick} />
      )}
</div>
...
```

### Rendering multiple components

```react
const numbers = [1, 2, 3, 4, 5];
const listItems = numbers.map((number) =>
  <li>{number}</li>
);
```

### Keys

```react
const todoItems = todos.map((todo) =>
  <li key={todo.id}>
    {todo.text}
  </li>
);
```

Only use keys in the context of the whole array. For example anytime map() is used, using of keys is advised. 

Keys only have to be unique beside siblings.

### Forms

Input fields such as <input> or <textarea> often manage their own state. We want a single source of truth in our own React state. We achieve this with

```react
class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Name:
          <input type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}
```

Change <input ...> to <textarea value={this.state.value} onChange={this.handleChange} /> to use a text box. No other changes are required. 

### Lifting State Up

The basic idea is that we are searching for a common parent of multible components. That parent becomes the single source of truth. It holds the state all components below it access. If for example our structure looks like this:

+ Filterable Page
  + Search Component
  + Table Component
    + Item 1
    + Item 2

We would choose the Filterable Page to be our single source of truth. It would hold the current value of the search field. All the other components only access it. For example the Table Component uses it to filter its items.

```react
//parent
constructor(){
    this.state = {filterText: ""};
    this.handleChange = this.handleChange.bind(this);
}

onChange(newText){
    this.setState({
      searchText: newText
    });
}
...

<SearchBar checked={this.state.inStockOnly} searchText={this.state.searchText} onChange={this.onChange}/>

//child
constructor(){
    this.handleChange = this.handleChange.bind(this);
}



onInputChange(e){
  this.props.onChange(e.target.value);
}

<input type="text" placeholder="Search..." onChange={this.onInputChange}/>


```

Child has no own state. It simply gets the parent method as a prop and calls it with the updated value. The parents then handles what to do with the new value. 

### Is my value a state?

+ Is it computable?
+ Is it a prop?
+ Is it constant?

=> then it's no state

### The use-Effect Method

```react
 /*
  Runs everytime the page renders 
  and everytime some node in the DOM is updated.
  */ 
  useEffect(()=>{
    console.log("Effect has been run :)");
  }, [counter]);

	// the last argument defines which state-changes make it run.
	// If the second argument is empty, it only runs when the 
	// application mounted.
```

### The Reducer in React

A simple Reducer Function:

``````react
const personReducer = (person, action) => {
  switch (action.type) {
    case 'INCREASE_AGE':
      return { ...person, age: person.age + 1 };
    case 'CHANGE_LASTNAME':
      return { ...person, lastname: action.lastname };
    default:
      return person;
  }
};
``````

person is our current state and action an object with a type property. Depeding on the last one the method decides what to do with the state.

Important is that the state parameter is immudable. We return a new state object and dont change the old one. The spread operator helps with that,  `{...person, lastname: "Peter"}` for exampler returns a new person object (our state) with one the lastname property changed.

In this example our action also has a so called **payload**. To change our lastname prop we need more information. Therefore the action object also hold a lastname prop. 



### The useReducer() function

```jsx
const [todos, dispatch] = React.useReducer(todoReducer, initialTodos);

dispatch({ type: 'DO_TODO', id: 'a' });
```

The useReducer() function takes our inital reducer function as well as our state. The returned dispatch function can now be called with our action (which has an type prop and in this case a payload prop). 